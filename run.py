from flask import Flask, render_template, request, jsonify
import json
from pprint import pprint
import urllib2
import requests

app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/build_config', methods=['GET', 'POST'])
def config():
    # Load json config
    with open('crawl_config.json') as data_file:
        data = json.load(data_file)
        data['include_urls'][0] = request.form['url']
        data['include_urls'][1] = request.form['url2']
        data['target'][0]['pattern']['url'] = request.form['url'] + '.*'
        data['target'][1]['pattern']['url'] = request.form['url2'] + '.*'
        data['urls'][0] = request.form['url']
        data['urls'][1] = request.form['url2']

        r = requests.post('http://localhost:9200/.river_web/config/my_web',
                          data=data)
    # url = request.form['url']
    return r.text


@app.route('/start_crawl', methods=['GET', 'POST'])
def crawl():
    index_name = request.form['index_name']
    with open('index_settings.json') as data_file:
        data = json.load(data_file)
        r = requests.post('http://localhost:9200/' + str(index_name),
                          data=data)
    return r.text


if __name__ == "__main__":
    app.run(debug=True)
